a <- 12/exp(12)
> help(sqrt)
> a<- (80:90)
> sum(a*a)
[1] 79585
> apropos("max",mode="function")
[1] "cummax"    "max"       "max.col"   "pmax"      "pmax.int"  "promax"    "varimax"   "which.max"
> setwd("C:\Users\Marek Wanted\Desktop\Semestr_1\Programowanie_R\Programowanie_R\Zad01")
Error: '\U' used without hex digits in character string starting ""C:\U"
> setwd("C:/Users/Marek Wanted/Desktop/Semestr_1/Programowanie_R/Programowanie_R/Zad01")
> a = "smartfon Motorola"
> save(a,file = "workspace.RData")
> remove(a)
> load("workspace.RData")
> install.packages("gridExtra")
Installing package into �C:/Users/Marek Wanted/Documents/R/win-library/3.5�
(as �lib� is unspecified)
trying URL 'https://cran.rstudio.com/bin/windows/contrib/3.5/gridExtra_2.3.zip'
Content type 'application/zip' length 1108400 bytes (1.1 MB)
downloaded 1.1 MB

package �gridExtra� successfully unpacked and MD5 sums checked

The downloaded binary packages are in
	C:\Users\Marek Wanted\AppData\Local\Temp\RtmpawoapK\downloaded_packages
> grid.table(t)
Error in grid.table(t) : could not find function "grid.table"
> install.packages("gridExtra")
Installing package into �C:/Users/Marek Wanted/Documents/R/win-library/3.5�
(as �lib� is unspecified)
trying URL 'https://cran.rstudio.com/bin/windows/contrib/3.5/gridExtra_2.3.zip'
Content type 'application/zip' length 1108400 bytes (1.1 MB)
downloaded 1.1 MB

package �gridExtra� successfully unpacked and MD5 sums checked

The downloaded binary packages are in
	C:\Users\Marek Wanted\AppData\Local\Temp\RtmpawoapK\downloaded_packages
> t <- head(Titanic[1:10])
> grid.table(t)
Error in grid.table(t) : could not find function "grid.table"
> vector = c(seq(400,320,-8))
> a = c(60:40)
> b = c(40:50)
> d = c(b,a)
> nazwa = c("Z4","E5","One Vision","G7","P30","One Dual","One Power","One","E4","Z2")
> wyswietlacz = c("4.5","5.3","5.4","6.0","7.3","4.0","4.0","5.2","5.9","5.75")
> pamiecRAM = c(4,4,5,4,6,5,4,3,5,4)
> pamiecWbudowana  = c(32,32,32,16,64,16,32,8,4,128)
> aparatFoto = c(8,8,8,8,8,12,16,12,4,20)
> cena = c(1000,2300,1234,1234,3000,2309,2090,1298,3094,1209)
> liczbaOpinii = c(10,12,23,198,20,31,42,53,3664,2)
> ramka <- data.frame(wyswietlacz,pamiecRAM,pamiecWbudowana,aparatFoto,cena,liczbaOpinii)
> mean(ramka$cena)
[1] 1876.8
> nowySmartfon = data.frame(
+ )
> nowySmartfon = data.frame(
+ wyswietlacz = "5.5",
+ pamiecRAM = 4,
+ pamiecWbudowana  = 256,
+ aparatFoto = 40,
+ cena = 3999,
+ liczbaOpinii = 400
+ )
> ramka <- data.frame(wyswietlacz,pamiecRAM,pamiecWbudowana,aparatFoto,cena,liczbaOpinii)
> nowySmartfon = data.frame(
+ wyswietlacz = "5.5",
+ pamiecRAM = 4,
+ pamiecWbudowana  = 256,
+ aparatFoto = 40,
+ cena = 3999,
+ liczbaOpinii = 400
+ )
> ramka
   wyswietlacz pamiecRAM pamiecWbudowana aparatFoto cena liczbaOpinii
1          4.5         4              32          8 1000           10
2          5.3         4              32          8 2300           12
3          5.4         5              32          8 1234           23
4          6.0         4              16          8 1234          198
5          7.3         6              64          8 3000           20
6          4.0         5              16         12 2309           31
7          4.0         4              32         16 2090           42
8          5.2         3               8         12 1298           53
9          5.9         5               4          4 3094         3664
10        5.75         4             128         20 1209            2
> mean(rama$cena)
Error in mean(rama$cena) : object 'rama' not found
> mean(ramka$cena)
[1] 1876.8
> ocenaKlientow = c(seq(0,5,0.5))
> ramka <-cbind(ramka,ocenaKlientow)
Error in data.frame(..., check.names = FALSE) : 
  argumenty sugeruj� r�n� liczb� wierszy: 10, 11
> ramka <- rbind(ramka,nowySmartfon)
> ramka
   wyswietlacz pamiecRAM pamiecWbudowana aparatFoto cena liczbaOpinii
1          4.5         4              32          8 1000           10
2          5.3         4              32          8 2300           12
3          5.4         5              32          8 1234           23
4          6.0         4              16          8 1234          198
5          7.3         6              64          8 3000           20
6          4.0         5              16         12 2309           31
7          4.0         4              32         16 2090           42
8          5.2         3               8         12 1298           53
9          5.9         5               4          4 3094         3664
10        5.75         4             128         20 1209            2
11         5.5         4             256         40 3999          400
> ramka <-cbind(ramka,ocenaKlientow)
> ramka
   wyswietlacz pamiecRAM pamiecWbudowana aparatFoto cena liczbaOpinii ocenaKlientow
1          4.5         4              32          8 1000           10           0.0
2          5.3         4              32          8 2300           12           0.5
3          5.4         5              32          8 1234           23           1.0
4          6.0         4              16          8 1234          198           1.5
5          7.3         6              64          8 3000           20           2.0
6          4.0         5              16         12 2309           31           2.5
7          4.0         4              32         16 2090           42           3.0
8          5.2         3               8         12 1298           53           3.5
9          5.9         5               4          4 3094         3664           4.0
10        5.75         4             128         20 1209            2           4.5
11         5.5         4             256         40 3999          400           5.0
> ramka
   wyswietlacz pamiecRAM pamiecWbudowana aparatFoto cena liczbaOpinii ocenaKlientow
1          4.5         4              32          8 1000           10           0.0
2          5.3         4              32          8 2300           12           0.5
3          5.4         5              32          8 1234           23           1.0
4          6.0         4              16          8 1234          198           1.5
5          7.3         6              64          8 3000           20           2.0
6          4.0         5              16         12 2309           31           2.5
7          4.0         4              32         16 2090           42           3.0
8          5.2         3               8         12 1298           53           3.5
9          5.9         5               4          4 3094         3664           4.0
10        5.75         4             128         20 1209            2           4.5
11         5.5         4             256         40 3999          400           5.0
> smart1 = data.frame(
+ wyswietlacz = "5",
+ pamiecRAM = 6,
+ pamiecWbudowana = 256,
+ aparatFoto = 8,
+ cena = 2000,
+ liczbaOpinii = 1000,
+ ocenaKlientow = 4.5
+ )
> smart2 = data.frame(
+ wyswietlacz = "5.5",
+ pamiecRAM = 4,
+ pamiecWbudowana = 256,
+ aparatFoto = 8,
+ cena = 2000,
+ liczbaOpinii = 1000,
+ ocenaKlientow = 4.5
+ )
> smart3 = data.frame(
+ wyswietlacz = "5.75",
+ pamiecRAM = 64,
+ pamiecWbudowana = 256,
+ aparatFoto = 8,
+ cena = 3000,
+ liczbaOpinii = 300,
+ ocenaKlientow = 4.5
+ )
> smart4 = data.frame(
+ wyswietlacz = "4.75",
+ pamiecRAM = 16,
+ pamiecWbudowana = 16,
+ aparatFoto = 4,
+ cena = 3000,
+ liczbaOpinii = 300,
+ ocenaKlientow = 4.5
+ )
> ramka <- rbind(ramka,smart1)
> ramka <- rbind(ramka,smart2)
> ramka <- rbind(ramka,smart3)
> ramka <- rbind(ramka,smart4)
> ramka
   wyswietlacz pamiecRAM pamiecWbudowana aparatFoto cena liczbaOpinii ocenaKlientow
1          4.5         4              32          8 1000           10           0.0
2          5.3         4              32          8 2300           12           0.5
3          5.4         5              32          8 1234           23           1.0
4          6.0         4              16          8 1234          198           1.5
5          7.3         6              64          8 3000           20           2.0
6          4.0         5              16         12 2309           31           2.5
7          4.0         4              32         16 2090           42           3.0
8          5.2         3               8         12 1298           53           3.5
9          5.9         5               4          4 3094         3664           4.0
10        5.75         4             128         20 1209            2           4.5
11         5.5         4             256         40 3999          400           5.0
12           5         6             256          8 2000         1000           4.5
13         5.5         4             256          8 2000         1000           4.5
14        5.75        64             256          8 3000          300           4.5
15        4.75        16              16          4 3000          300           4.5
> install.packages("plotfix")
Installing package into �C:/Users/Marek Wanted/Documents/R/win-library/3.5�
(as �lib� is unspecified)
Warning in install.packages :
  package �plotfix� is not available (for R version 3.5.2)
Error in loadNamespace(name) : there is no package called �yaml�
Error in loadNamespace(name) : there is no package called �yaml�
Error in loadNamespace(name) : there is no package called �yaml�
Error in loadNamespace(name) : there is no package called �yaml�
Error in loadNamespace(name) : there is no package called �yaml�
Error in loadNamespace(name) : there is no package called �yaml�
Error in loadNamespace(name) : there is no package called �yaml�
Error in loadNamespace(name) : there is no package called �yaml�
Error in loadNamespace(name) : there is no package called �yaml�
Error in loadNamespace(name) : there is no package called �yaml�
Error in loadNamespace(name) : there is no package called �yaml�
Error in loadNamespace(name) : there is no package called �yaml�
Error in loadNamespace(name) : there is no package called �yaml�
Error in loadNamespace(name) : there is no package called �yaml�
> barplot(max(ramka$liczbaOpinii),)
> barplot(ramka)
Error in barplot.default(ramka) : 'height' musi by� wektorem lub macierz�
> barplot(ramka$liczbaOpinii)
> barplot([10:100])
Error: unexpected '[' in "barplot(["
> barplot(10:100)
> help(barplot)
> ramka$liczbaOpinii
 [1]   10   12   23  198   20   31   42   53 3664    2  400 1000 1000  300  300
> as.vector(ramka$liczbaOpinii)
 [1]   10   12   23  198   20   31   42   53 3664    2  400 1000 1000  300  300
> barplot(as.vector(ramka$liczbaOpinii))
> ocena = seq(0,5,0.5)
> licznosc=  ocena(ramka$ocenaKlientow)
Error in ocena(ramka$ocenaKlientow) : could not find function "ocena"
> licznosc=  table(ramka$ocenaKlientow)
> licznosc

  0 0.5   1 1.5   2 2.5   3 3.5   4 4.5   5 
  1   1   1   1   1   1   1   1   1   5   1 
> ramka
   wyswietlacz pamiecRAM pamiecWbudowana aparatFoto cena liczbaOpinii ocenaKlientow
1          4.5         4              32          8 1000           10           0.0
2          5.3         4              32          8 2300           12           0.5
3          5.4         5              32          8 1234           23           1.0
4          6.0         4              16          8 1234          198           1.5
5          7.3         6              64          8 3000           20           2.0
6          4.0         5              16         12 2309           31           2.5
7          4.0         4              32         16 2090           42           3.0
8          5.2         3               8         12 1298           53           3.5
9          5.9         5               4          4 3094         3664           4.0
10        5.75         4             128         20 1209            2           4.5
11         5.5         4             256         40 3999          400           5.0
12           5         6             256          8 2000         1000           4.5
13         5.5         4             256          8 2000         1000           4.5
14        5.75        64             256          8 3000          300           4.5
15        4.75        16              16          4 3000          300           4.5
> licznosc=  table(ramka$ocenaKlientow*ramka$liczbaOpinii)
> licznosc

    0     6     9    23    40  77.5   126 185.5   297  1350  2000  4500 14656 
    1     1     1     1     1     1     1     1     1     2     1     2     1 
> licznosc=  table(ramka$ocenaKlientow)
> licznosc

  0 0.5   1 1.5   2 2.5   3 3.5   4 4.5   5 
  1   1   1   1   1   1   1   1   1   5   1 
> barplot(licznosc)